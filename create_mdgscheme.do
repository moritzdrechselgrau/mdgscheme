set scheme s2color
grstyle init mdgscheme, path("C:\ado\personal") replace

// Meta
local mygray gs12
local mypalette mdgBRGG

// Size
grstyle graphsize x           3.575
grstyle graphsize y           2.6

// Background colors
grstyle color background             white
grstyle color plotregion             white
grstyle color matrix_plotregion      white
grstyle color legend                 white%80

// Legends
grstyle gridringstyle legend_ring 			0
grstyle clockdir 			legend_position 	2
grstyle color 				legend 						white%80
grstyle numstyle 			legend_cols 			1
grstyle gsize 				legend_key_xsize  medsmall
grstyle gsize 				legend_key_ysize  small
grstyle gsize 				key_label  				small

// Grid Lines
grstyle set plain, grid //minor //horizontal 
grstyle yesno 		draw_major_hgrid 	yes
grstyle yesno 		draw_major_vgrid  yes
grstyle yesno 		grid_draw_min 		yes
grstyle yesno 		grid_draw_max 		yes
grstyle color 		major_grid 				gs12
grstyle linewidth major_grid 				vthin
grstyle color 		minor_grid 				gs12
grstyle linewidth minor_grid 				vthin

// Axis Lines 
grstyle linepattern axisline 					blank
grstyle axisstyle 	bar_scale_vert 		none
grstyle axisstyle 	bar_scale_horiz 	none
grstyle gsize 			axis_title_gap 		medlarge
grstyle axisstyle 	bar_scale_vert 		none
grstyle axisstyle 	bar_scale_horiz 	none

// Ticks
grstyle numticks_g horizontal_major  5
grstyle numticks_g vertical_major    5
grstyle anglestyle 	vertical_tick 	horizontal
grstyle color 			tick 						gs12
grstyle gsize 			tickgap 				tiny
grstyle gsize 			tick 						zero
grstyle color 			minortick 			gs12
grstyle gsize 			minortick 			zero

// Markers
grstyle symbolsize 	p			medsmall

// Legend
grstyle gridringstyle legend_ring 			0
grstyle clockdir 			legend_position 	2
grstyle color 				legend 						white%80
grstyle numstyle 			legend_cols 			1
grstyle gsize 				key_label  				small
grstyle gsize 				legend_key_ysize  small
grstyle gsize 				legend_key_xsize  huge

// Colors
grstyle set color `mypalette': p#
grstyle set color `mypalette', opacity(80): 	p#markfill
grstyle set color `mypalette', opacity(75):	histogram
grstyle set color `mypalette': histogram_line
grstyle color ci_area gs5%20
grstyle color ci2_area gs5%20

// Textboxes 
grstyle color textbox          	gs14
grstyle color bylabel_outline  	gs14
grstyle color heading						black 

// Font
graph set window fontface default

/*
* Example
* =========================================
sysuse auto, clear
sort weight

twoway (connect price weight if foreign == 0) || (connect price weight if foreign == 1), ytitle(Price) legend(on label(1 "Domestic") label(2 "Foreign")) name(connect, replace)
graph bar price weight, over(foreign) name(bar, replace)
binscatter price weight, name(binscatter, replace) color(%100..)
twoway histogram price, name(histogram, replace) by(foreign, title("Title"))
twoway (lfitci price weight if foreign == 1) (lfitci price weight if foreign == 0)
*/